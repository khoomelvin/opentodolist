<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../Pages/AboutPage.qml" line="14"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="31"/>
        <source>OpenTodoList</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="38"/>
        <source>A todo and task managing application.</source>
        <translation type="unfinished">Une application de gestion des tâches et des tâches.</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="45"/>
        <source>(c) RPdev 2013-2020, version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="60"/>
        <source>OpenTodoList is released under the terms of the GNU General Public License version 3 or (at your choice) any later version. You can find a copy of the license below. Additionally, several libraries and resources are used. For detailed information about their license terms, please refer to the &lt;a href=&apos;3rdparty&apos;&gt;3rd Party Software&lt;/a&gt; page.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutThirdPartyPage</name>
    <message>
        <location filename="../Pages/AboutThirdPartyPage.qml" line="11"/>
        <source>Used Libraries and Resources</source>
        <translation type="unfinished">Bibliothèques et ressources d&apos;occasion</translation>
    </message>
    <message>
        <location filename="../Pages/AboutThirdPartyPage.qml" line="47"/>
        <source>Author: &lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/AboutThirdPartyPage.qml" line="54"/>
        <source>License: &lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/AboutThirdPartyPage.qml" line="61"/>
        <source>Download: &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Attachments</name>
    <message>
        <location filename="../Widgets/Attachments.qml" line="28"/>
        <source>Attach File</source>
        <translation type="unfinished">Fichier joint</translation>
    </message>
    <message>
        <location filename="../Widgets/Attachments.qml" line="41"/>
        <source>Delete Attachment?</source>
        <translation type="unfinished">Supprimer pièce jointe ?</translation>
    </message>
    <message>
        <location filename="../Widgets/Attachments.qml" line="45"/>
        <source>Are you sure you want to delete the attachment &lt;strong&gt;%1&lt;/strong&gt;? This action cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/Attachments.qml" line="59"/>
        <source>Attachments</source>
        <translation type="unfinished">Attachments</translation>
    </message>
</context>
<context>
    <name>ColorMenu</name>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="15"/>
        <source>Color</source>
        <translation type="unfinished">Couleur</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="18"/>
        <source>White</source>
        <translation type="unfinished">Blanc</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="25"/>
        <source>Red</source>
        <translation type="unfinished">Rouge</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="32"/>
        <source>Green</source>
        <translation type="unfinished">Vert</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="39"/>
        <source>Blue</source>
        <translation type="unfinished">Bleu</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="46"/>
        <source>Yellow</source>
        <translation type="unfinished">Jaune</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="53"/>
        <source>Orange</source>
        <translation type="unfinished">Orange</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="60"/>
        <source>Lilac</source>
        <translation type="unfinished">Lilas</translation>
    </message>
</context>
<context>
    <name>Colors</name>
    <message>
        <location filename="../Utils/Colors.qml" line="16"/>
        <source>System</source>
        <translation type="unfinished">Système</translation>
    </message>
    <message>
        <location filename="../Utils/Colors.qml" line="17"/>
        <source>Light</source>
        <translation type="unfinished">Clair</translation>
    </message>
    <message>
        <location filename="../Utils/Colors.qml" line="18"/>
        <source>Dark</source>
        <translation type="unfinished">Sombre</translation>
    </message>
</context>
<context>
    <name>DeleteItemDialog</name>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="19"/>
        <source>Delete Item?</source>
        <translation type="unfinished">Supprimer un élément ?</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="32"/>
        <source>Do you really want to delete the image &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="36"/>
        <source>Do you really want to delete the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="40"/>
        <source>Do you really want to delete the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="44"/>
        <source>Do you really want to delete the task &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="48"/>
        <source>Do you really want to delete the note &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteLibraryDialog</name>
    <message>
        <location filename="../Windows/DeleteLibraryDialog.qml" line="19"/>
        <source>Delete Library?</source>
        <translation type="unfinished">Supprimer la bibliothèque ?</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteLibraryDialog.qml" line="33"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from  the application? &lt;em&gt;This will remove any files belonging to the library.&lt;/em&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/DeleteLibraryDialog.qml" line="40"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from the application? Note that the files inside the library will not be removed, so you can restore the library later on.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FolderSelectionDialog</name>
    <message>
        <location filename="../Windows/FolderSelectionDialog.qml" line="17"/>
        <source>Select a Folder</source>
        <translation type="unfinished">Sélectionnez un dossier</translation>
    </message>
</context>
<context>
    <name>ItemCreatedNotification</name>
    <message>
        <location filename="../Widgets/ItemCreatedNotification.qml" line="64"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt; has been created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/ItemCreatedNotification.qml" line="69"/>
        <source>Open</source>
        <translation type="unfinished">Ouvrir</translation>
    </message>
</context>
<context>
    <name>ItemDueDateEditor</name>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="30"/>
        <source>Due on</source>
        <translation type="unfinished">Dû le</translation>
    </message>
</context>
<context>
    <name>ItemNotesEditor</name>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="28"/>
        <source>Notes</source>
        <translation type="unfinished">Notes sur les</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="60"/>
        <source>No notes added yet - click here to add some.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibrariesSideBar</name>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="109"/>
        <source>Schedule</source>
        <translation type="unfinished">Calendrier</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="146"/>
        <source>New Library</source>
        <translation type="unfinished">Nouvelle bibliothèque</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="152"/>
        <source>Settings</source>
        <translation type="unfinished">Paramètres</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="159"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="171"/>
        <source>Create Default Library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryPage</name>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="137"/>
        <source>Red</source>
        <translation type="unfinished">Rouge</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="144"/>
        <source>Green</source>
        <translation type="unfinished">Vert</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="151"/>
        <source>Blue</source>
        <translation type="unfinished">Bleu</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="158"/>
        <source>Yellow</source>
        <translation type="unfinished">Jaune</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="165"/>
        <source>Orange</source>
        <translation type="unfinished">Orange</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="172"/>
        <source>Lilac</source>
        <translation type="unfinished">Lilas</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="179"/>
        <source>White</source>
        <translation type="unfinished">Blanc</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="189"/>
        <source>Rename</source>
        <translation type="unfinished">Renommer</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="194"/>
        <source>Delete</source>
        <translation type="unfinished">Supprimer</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="252"/>
        <source>Note Title</source>
        <translation type="unfinished">Note Titre</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="265"/>
        <source>Todo List Title</source>
        <translation type="unfinished">Titre de la liste de tâches</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="278"/>
        <source>Search term 1, search term 2, ...</source>
        <translation type="unfinished">Terme de recherche 1, terme de recherche 2, ....</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="344"/>
        <source>Nothing here yet! Start by adding a &lt;a href=&apos;#note&apos;&gt;note&lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;todo list&lt;/a&gt; or &lt;a href=&apos;#image&apos;&gt;image&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="413"/>
        <source>Sort By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="417"/>
        <source>Manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="424"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="431"/>
        <source>Due To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="438"/>
        <source>Created At</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="445"/>
        <source>Updated At</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryPageMenu</name>
    <message>
        <location filename="../Menues/LibraryPageMenu.qml" line="18"/>
        <source>Edit Sync Settings</source>
        <translation type="unfinished">Modifier les paramètres de synchronisation</translation>
    </message>
    <message>
        <location filename="../Menues/LibraryPageMenu.qml" line="34"/>
        <source>Sync Now</source>
        <translation type="unfinished">Synchroniser maintenant</translation>
    </message>
    <message>
        <location filename="../Menues/LibraryPageMenu.qml" line="45"/>
        <source>Sync Log</source>
        <translation type="unfinished">Synchroniser le journal</translation>
    </message>
</context>
<context>
    <name>LibrarySecretsMissingNotificationBar</name>
    <message>
        <location filename="../Widgets/LibrarySecretsMissingNotificationBar.qml" line="36"/>
        <source>The secrets for synchronizing the library %1 are missing.</source>
        <translation type="unfinished">Les secrets pour synchroniser la bibliothèque %1 sont manquants.</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrarySecretsMissingNotificationBar.qml" line="41"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrarySecretsMissingNotificationBar.qml" line="45"/>
        <source>Enter Secrets</source>
        <translation type="unfinished">Entrez Secrets</translation>
    </message>
</context>
<context>
    <name>LogViewPage</name>
    <message>
        <location filename="../Pages/LogViewPage.qml" line="13"/>
        <source>Synchronization Log</source>
        <translation type="unfinished">Journal de synchronisation</translation>
    </message>
    <message>
        <location filename="../Pages/LogViewPage.qml" line="21"/>
        <source>Copy Log</source>
        <translation type="unfinished">Copier le journal</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Windows/MainWindow.qml" line="21"/>
        <source>OpenTodoList</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="189"/>
        <source>Rename</source>
        <translation type="unfinished">Renommer</translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="200"/>
        <source>Color</source>
        <translation type="unfinished">Couleur</translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="211"/>
        <source>Add Tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="218"/>
        <source>Attach File</source>
        <translation type="unfinished">Fichier joint</translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="227"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="234"/>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="242"/>
        <source>Set Due Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="250"/>
        <source>Delete</source>
        <translation type="unfinished">Supprimer</translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="288"/>
        <source>Settings</source>
        <translation type="unfinished">Paramètres</translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="289"/>
        <source>Ctrl+,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="296"/>
        <source>New &amp;Library</source>
        <translation type="unfinished">Nouveau &amp;Bibliothèque</translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="307"/>
        <source>New &amp;Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="315"/>
        <source>New &amp;Todo List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="323"/>
        <source>New &amp;Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="331"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="339"/>
        <source>&amp;Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="366"/>
        <source>Open Last &amp;Created Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="374"/>
        <source>Open &amp;Left Side Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="482"/>
        <source>Start by &lt;a href=&apos;#newLibrary&apos;&gt;creating a new library&lt;/a&gt;. Libraries are used to store different kinds of items like notes, todo lists and images.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewLibraryPage</name>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="19"/>
        <source>Create Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="23"/>
        <source>Create</source>
        <translation type="unfinished">Créer</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="39"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuler</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="58"/>
        <source>Library Name:</source>
        <translation type="unfinished">Nom de la bibliothèque :</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="66"/>
        <source>My New Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="73"/>
        <source>Create Library in Default Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="80"/>
        <source>The items you add to the library will be stored in the default location for libraries.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="82"/>
        <source>Please select a directory into which the library items shall be saved. You can also select an existing library directory. In this case, the library will be imported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="110"/>
        <source>Please select a library location</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewTopLevelItemButton</name>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="38"/>
        <source>Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="42"/>
        <source>Todo List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="46"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenFileDialog</name>
    <message>
        <location filename="../Windows/OpenFileDialog.qml" line="18"/>
        <source>Select a File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenImageDialog</name>
    <message>
        <location filename="../Windows/+ios/OpenImageDialog.qml" line="19"/>
        <location filename="../Windows/OpenImageDialog.qml" line="19"/>
        <source>Select Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenameItemDialog</name>
    <message>
        <location filename="../Windows/RenameItemDialog.qml" line="23"/>
        <source>Rename Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/RenameItemDialog.qml" line="34"/>
        <source>Enter item title...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenameLibraryDialog</name>
    <message>
        <location filename="../Windows/RenameLibraryDialog.qml" line="20"/>
        <source>Rename Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/RenameLibraryDialog.qml" line="37"/>
        <source>Enter library title...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScheduleViewPage</name>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="71"/>
        <source>Overdue</source>
        <translation type="unfinished">En retard</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="127"/>
        <source>Today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="128"/>
        <source>Tomorrow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="132"/>
        <source>Later This Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="134"/>
        <source>Next Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="135"/>
        <source>Coming Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="251"/>
        <source>Nothing scheduled... Add a due date to items for them to appear here.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPageForm.ui</name>
    <message>
        <location filename="../Pages/SettingsPageForm.ui.qml" line="32"/>
        <source>Settings</source>
        <translation type="unfinished">Paramètres</translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPageForm.ui.qml" line="38"/>
        <source>User Interface</source>
        <translation type="unfinished">Interface utilisateur</translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPageForm.ui.qml" line="46"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPageForm.ui.qml" line="55"/>
        <source>Theme:</source>
        <translation type="unfinished">Thème:</translation>
    </message>
</context>
<context>
    <name>SyncErrorNotificationBar</name>
    <message>
        <location filename="../Widgets/SyncErrorNotificationBar.qml" line="43"/>
        <source>There were errors when synchronizing the library. Please ensure that the library settings are up to date.</source>
        <translation type="unfinished">Il y a eu des erreurs lors de la synchronisation de la bibliothèque. Veuillez vous assurer que les paramètres de la bibliothèque sont à jour.</translation>
    </message>
    <message>
        <location filename="../Widgets/SyncErrorNotificationBar.qml" line="49"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/SyncErrorNotificationBar.qml" line="53"/>
        <source>View</source>
        <translation type="unfinished">Afficher</translation>
    </message>
</context>
<context>
    <name>SyncIndicatorBar</name>
    <message>
        <location filename="../Widgets/SyncIndicatorBar.qml" line="63"/>
        <source>Synchronizing library...</source>
        <translation type="unfinished">Bibliothèque de synchronisation....</translation>
    </message>
</context>
<context>
    <name>SyncLibrarySelectionPage</name>
    <message>
        <location filename="../Pages/SyncLibrarySelectionPage.qml" line="17"/>
        <source>Create Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SyncLibrarySelectionPage.qml" line="29"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SyncLibrarySelectionPage.qml" line="46"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuler</translation>
    </message>
    <message>
        <location filename="../Pages/SyncLibrarySelectionPage.qml" line="79"/>
        <source>Create new library on server</source>
        <translation type="unfinished">Créer une nouvelle bibliothèque sur le serveur</translation>
    </message>
    <message>
        <location filename="../Pages/SyncLibrarySelectionPage.qml" line="97"/>
        <source>Searching for existing libraries...</source>
        <translation type="unfinished">Recherche de bibliothèques existantes...</translation>
    </message>
</context>
<context>
    <name>SynchronizerBackendSelectionPage</name>
    <message>
        <location filename="../Pages/SynchronizerBackendSelectionPage.qml" line="16"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuler</translation>
    </message>
    <message>
        <location filename="../Pages/SynchronizerBackendSelectionPage.qml" line="21"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SynchronizerBackendSelectionPage.qml" line="45"/>
        <source>NextCloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SynchronizerBackendSelectionPage.qml" line="52"/>
        <source>ownCloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SynchronizerBackendSelectionPage.qml" line="59"/>
        <location filename="../Pages/SynchronizerBackendSelectionPage.qml" line="60"/>
        <source>WebDAV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SynchronizerBackendSelectionPage.qml" line="66"/>
        <location filename="../Pages/SynchronizerBackendSelectionPage.qml" line="67"/>
        <source>Local Library</source>
        <translation type="unfinished">Bibliothèque locale</translation>
    </message>
</context>
<context>
    <name>TagsEditor</name>
    <message>
        <location filename="../Widgets/TagsEditor.qml" line="34"/>
        <source>Add Tag</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TodoListItem</name>
    <message>
        <location filename="../Widgets/TodoListItem.qml" line="95"/>
        <source>✔ No open todos - everything done</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TodoListPage</name>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="167"/>
        <source>Search term 1, search term 2, ...</source>
        <translation type="unfinished">Terme de recherche 1, terme de recherche 2, ....</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="216"/>
        <source>Todos</source>
        <translation type="unfinished">Todos</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="234"/>
        <source>Add new todo...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="256"/>
        <source>Manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="262"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="268"/>
        <source>Due Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="274"/>
        <source>Created At</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="280"/>
        <source>Updated At</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="290"/>
        <source>Completed Todos</source>
        <translation type="unfinished">Todos achevés</translation>
    </message>
</context>
<context>
    <name>TodoPage</name>
    <message>
        <location filename="../Pages/TodoPage.qml" line="100"/>
        <source>Search term 1, search term 2, ...</source>
        <translation type="unfinished">Terme de recherche 1, terme de recherche 2, ....</translation>
    </message>
    <message>
        <location filename="../Pages/TodoPage.qml" line="139"/>
        <source>Tasks</source>
        <translation type="unfinished">Étranger</translation>
    </message>
    <message>
        <location filename="../Pages/TodoPage.qml" line="142"/>
        <source>Add new task...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TodosWidget</name>
    <message>
        <location filename="../Widgets/TodosWidget.qml" line="141"/>
        <source>Due on: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/TodosWidget.qml" line="271"/>
        <source>Swipe to mark undone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/TodosWidget.qml" line="272"/>
        <source>Swipe to mark done</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Translations</name>
    <message>
        <location filename="../Utils/Translations.qml" line="14"/>
        <source>System Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Utils/Translations.qml" line="18"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Utils/Translations.qml" line="22"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Utils/Translations.qml" line="26"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateNotificationBar</name>
    <message>
        <location filename="../Widgets/UpdateNotificationBar.qml" line="44"/>
        <source>An update to OpenTodoList %1 is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/UpdateNotificationBar.qml" line="50"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/UpdateNotificationBar.qml" line="54"/>
        <source>Download</source>
        <translation type="unfinished">Télécharger</translation>
    </message>
</context>
<context>
    <name>WebDAVConnectionSettingsPage</name>
    <message>
        <location filename="../Pages/WebDAVConnectionSettingsPage.qml" line="22"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/WebDAVConnectionSettingsPage.qml" line="22"/>
        <source>Validate</source>
        <translation type="unfinished">Valider</translation>
    </message>
    <message>
        <location filename="../Pages/WebDAVConnectionSettingsPage.qml" line="40"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuler</translation>
    </message>
    <message>
        <location filename="../Pages/WebDAVConnectionSettingsPage.qml" line="86"/>
        <source>Server Address:</source>
        <translation type="unfinished">Adresse du serveur :</translation>
    </message>
    <message>
        <location filename="../Pages/WebDAVConnectionSettingsPage.qml" line="92"/>
        <source>https://...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/WebDAVConnectionSettingsPage.qml" line="98"/>
        <source>User name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/WebDAVConnectionSettingsPage.qml" line="110"/>
        <source>Password:</source>
        <translation type="unfinished">Mot de passe:</translation>
    </message>
    <message>
        <location filename="../Pages/WebDAVConnectionSettingsPage.qml" line="124"/>
        <source>Ignore SSL Errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/WebDAVConnectionSettingsPage.qml" line="130"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WebDAVSynchronizerSettingsPage</name>
    <message>
        <location filename="../Pages/WebDAVSynchronizerSettingsPage.qml" line="40"/>
        <source>User name</source>
        <translation type="unfinished">Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../Pages/WebDAVSynchronizerSettingsPage.qml" line="53"/>
        <source>Password</source>
        <translation type="unfinished">Mot de passe</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="98"/>
        <source>Manage your personal data.</source>
        <translation type="unfinished">Gérer vos données personnelles.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="104"/>
        <source>QML Root Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="105"/>
        <source>DIR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="111"/>
        <source>Switch on some optimizations for touchscreens.</source>
        <translation type="unfinished">Activez certaines optimisations pour les écrans tactiles.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="124"/>
        <source>Remove shortcuts to the AppImage.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
